/* d3.json("http://dl.dropbox.com/u/2386961/govhack/members.comparison.json", function(jsonData) { */
d3.json("/json/members.comparison.json", function(jsonData) {

  if (jsonData.hasOwnProperty("members")) {

    // extract total word count for this lot
    var word_count_member = 0;
    var word_count_total  = jsonData.word_count_total;

    // members
    var members_compare = d3.select("#members_compare").selectAll("div")
      .data(jsonData.members);
        
    // member node
    var member = members_compare
      .enter()
        .append("div")
          .attr("class", function(d, i) { return (i%2 == 0) ? 'member alpha' : ' member beta'; });
    
    // member name
    member
      .append("div")
        .attr("class", "member_name")
        .text(function(d, i) { return d.name; });
        
    // word count
    member
      .append("div")
        .attr("class", "member_word_count")
        .text(function(d, i) {
          word_count_member = d.word_count;
          var word_count_percentage = d.word_count / word_count_total * 100;
          return word_count_percentage + '%';
        });
          
    // member image
    member
      .append("div")
        .attr("class", "image")
          .append("img")
            .attr("src", function(d, i) { return d.image; })
          
    // member topics
    var member_topics = member
      .append("div")
        .attr("class", "topics")
        .selectAll("div")
          .data(function(d) { return d.topics; });
          
    // member topic
    var member_topic = member_topics
      .enter()
        .append("div")
          .attr("class", "topic");
          
    // member topic name
    member_topic
      .append("div")
        .attr("class", "topic_name")
        .text(function(d, i) { return d.name; });
          
    // member topic word count
    member_topic
      .append("div")
        .attr("class", "topic_word_count")
        .text(function(d, i) {
          var word_count_percentage = d.word_count / word_count_member * 100;
          return word_count_percentage + '%';
        });
        
    // member topic sentiments
    var member_topic_sentiments = member_topic
      .append("div")
        .attr("class", "topic_sentiments")
        .selectAll("div")
          .data(function(d) { return d.sentiments; });
          
    // member topic sentiment
    // @todo - need to do the width here based on the value
    var member_topic_sentiment = member_topic_sentiments
      .enter()
        .append("div")
          .attr("class", function(d, i) { return "topic_sentiment " + d.name; })
          .text(function(d, i) { return d.name; });
          
    // member favourite phrases
    var member_favourite_phrases = member
      .append("div")
        .attr("class", "favourite_phrases")
        .selectAll("div")
          .data(function(d) { return d.favourite_phrases; });
          
    // member favourite phrases
    // @todo - need soe kind of basic tag cloud for this
    var member_favourite_phrase = member_favourite_phrases
      .enter()
        .append("div")
          .attr("class", "favourite_phrase")
          .style("font-size", function(d, i) {
            // do something based on d.count
            return "11px";
          })
          .text(function(d, i) { return d.name; });
          
    // bills passed or failed
    var member_bills = member
      .append("div")
        .attr("class", "bills");
          
    // bills helped to pass
    // @todo - do the pie chart once you've got the rest of the things in place
    var member_bills_passed = member_bills
      .append("div")
        .attr("class", "bills_passed")
        .text(function(d, i) {
          return d.bills.voted_for.passed + " " + d.bills.voted_for.failed;
        });
          
  }
});